$("body").ready(function() {
    $(".card__link").click(function() {
        $(".modal__acquisition").addClass("modal--show");
    });

    $("#info").click(function() {
        $(".modal__doubts").addClass("modal--show");
    });

    $(".modal__close").click(function() {
        $(".modal").removeClass("modal--show");
    });

    $("#acquisition").click(function() {
        $(".modal__acquisition").removeClass("modal--show");
        $(".modal__confirmation").addClass("modal--show");
    });

    $(".card__arrow").click(function() {
        $(this).closest(".card").find(".card__options").toggle();
    });

    $(".edit-limits").click(function(){
        $(".modal__limits").addClass("modal--show");
    });

    $(".reset-password").click(function(){
        $(".modal__password").addClass("modal--show");
    });
});
