var gulp = require('gulp'),
	sass = require('gulp-sass'),
	watch = require('gulp-watch'),
	browserSync = require('browser-sync').create(),
    sourcemaps = require('gulp-sourcemaps');


// BroserSync
gulp.task('serve', function() {
    browserSync.init({
        server: {
            baseDir: "./src"
        }
    });

    gulp.watch("src/sass/*.scss", ['sass']);
    gulp.watch("src/*.html").on('change', browserSync.reload);
});

// sass
gulp.task('sass', function(){
	gulp.src('src/sass/*.scss')
    .pipe(sourcemaps.init())
	.pipe(sass())
    .pipe(sourcemaps.write('./maps'))
	.pipe(gulp.dest('src/css/'))
	.pipe(browserSync.stream());
});

gulp.task('default', ['serve']);
